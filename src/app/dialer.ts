export class Dialer {
    usersNumber: string;
    constructor(usersNumber) {
        this.usersNumber = usersNumber.toString();
    }

    addChar(addedChar) {
        /* console.log(addedChar); */
        this.usersNumber += addedChar;
    }
    removeLastChar() {
        this.usersNumber = this.usersNumber.slice(0, this.usersNumber.length - 1);
    }
    reset() {
        this.usersNumber = "";
    }
    call() {

        console.log('working: ' + this.usersNumber)
        this.reset();
        return
    }
}