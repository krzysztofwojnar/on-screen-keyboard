import { Component } from '@angular/core';
import { Dialer } from './dialer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  dialer: Dialer = new Dialer("");
}
